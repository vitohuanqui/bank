package repository;

import java.util.Collection;

import domain.Account;
import domain.Banco;

public interface BancoRepository extends BaseRepository<Banco, Long> {
	Banco findById(Long bancoId);
}

