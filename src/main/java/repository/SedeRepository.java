package repository;

import java.util.Collection;

import domain.Banco;
import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long> {
	Sede findByID(Long id);

	Collection<Sede> findByBancoID(Long bancoId);
}
