package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AccountRepository;
import repository.SedeRepository;
import domain.Account;
import domain.Person;
import domain.Sede;
import domain.Banco;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
SedeRepository {
	@Override
	public Sede findByID(Long id){
		String jpaQuery = "SELECT a FROM Sede a WHERE a.id = :id";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("id", id);
		return getFirstResult(query);
	}

	@Override
	public Collection<Sede> findByBancoID(Long bancoId) {
		String jpaQuery = "SELECT a FROM Sede a JOIN a.banco p WHERE p.id = :bancoId";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("personId", bancoId);
		return query.getResultList();
	}
	
}
